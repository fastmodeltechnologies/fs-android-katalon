import com.kms.katalon.core.annotation.BeforeTestCase
import com.kms.katalon.core.annotation.BeforeTestSuite
import com.kms.katalon.core.annotation.AfterTestCase
import com.kms.katalon.core.annotation.AfterTestSuite
import com.kms.katalon.core.context.TestCaseContext
import com.kms.katalon.core.context.TestSuiteContext
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.util.internal.PathUtil as PathUtil
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import io.appium.java_client.AppiumDriver
import io.appium.java_client.MobileElement
import io.appium.java_client.service.local.AppiumDriverLocalService
import io.appium.java_client.service.local.AppiumServiceBuilder
import io.appium.java_client.service.local.flags.GeneralServerFlag

class ListenerConfig {
	
	public AppiumDriverLocalService service;
	@BeforeTestCase
	def BeforeTestSuite(TestSuiteContext testSuiteContext) {
		service = AppiumDriverLocalService.buildDefaultService()
		service.start()
		Mobile.comment("Is Appium server running: "+service.isRunning());
		Mobile.comment("*************** Initializing Before TestSuite ***************")
		FileInputStream reader = new FileInputStream('./config.properties');
		Properties properties = new Properties();
		properties.load(reader);
		String environment = properties.getProperty('environment')
		
		switch (environment) {
			
			case "PROD":
			Mobile.comment("***** Launching PROD Env *******");
				
			break;
			case "QA":
			Mobile.comment("************ Launching QA Env *********** ");
			
			break;
		}
		def String data = properties.getProperty('environment')
		GlobalVariable.data = data
		
		int device_Height,device_Width;
		
		'Path of the Apk File Store in path variable'
		def path = RunConfiguration.getProjectDir() + '/Data Files/FastScoutApp.apk'
		 
		if (properties.getProperty('installApp') == "No") {
		Mobile.startExistingApplication("com.fastmodelsports.fastscout")
		AppiumDriver<MobileElement> driver = MobileDriverFactory.getDriver()
		} 
			else if (properties.getProperty('installApp') == "Yes") {
			'Start the application'
			Mobile.startApplication(path, true)
			
			AppiumDriver<MobileElement> driver = MobileDriverFactory.getDriver()
			
			Mobile.tapAndHold(findTestObject('SignIn/signInBtn'), 0, 2)
			
			Mobile.tap(GlobalVariable.Envi, 0)
			
			Mobile.tap(findTestObject('SignIn/signInBtn'), 0)
			
			Mobile.delay(5)
			
			Mobile.setText(findTestObject('SignIn/email'), GlobalVariable.email, 0)
			
			Mobile.setText(findTestObject('SignIn/password'), GlobalVariable.password, 0)
			
			Mobile.delay(5)
			
			Mobile.tap(findTestObject('SignIn/signInBtn2'), 0)
			
			Mobile.waitForElementPresent(findTestObject('SignIn/licenseAgreement'), 45)
			
			'Get Device Height and Store in device_height variable'
			device_Height = Mobile.getDeviceHeight()
			
			'Get Width Height and Store in device_Width variable'
			device_Width = Mobile.getDeviceWidth()
			
			'Storing the startX value by dividing device width by 2. Because x coordinates are constant for Vertical Swiping'
			int startX = device_Width / 2
			
			'Here startX and endX values are equal for vertical Swiping for that assigning startX value to endX'
			int endX = startX
			
			'Storing the startY value'
			int startY = device_Height * 0.30
			
			'Storing the endY value'
			int endY = device_Height * 0.80
			
		//	Mobile.scrollToText('or advertising relating to the Software.') - not working
			'Swipe Vertical from top to bottom'
			for(int i=0;i<12;i++)
			{
			Mobile.swipe(startX, endY, endX, startY)
			
			MobileBuiltInKeywords.delay(2)
			}
			
			Mobile.delay(5)
			
			Mobile.tap(findTestObject('SignIn/agreeBtn'), 0)
			
			Mobile.tap(findTestObject('SignIn/gotItBtn'), 2)
			}
	
	}

	@AfterTestCase
	def AfterTestSuite(TestSuiteContext testSuiteContext) {
		
		Mobile.comment("********* Executing After Test Suite**********");
		
//		Mobile.tap(findTestObject('Scouts/SignoutBtn'), 0)
//		
//		Mobile.pressBack()
//		
//		MobileBuiltInKeywords.delay(3)
//		
//		Mobile.closeApplication()
		service.stop()
			}
}