<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>RegressionTest</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>34aa5b39-60e0-4e39-89c6-840a40041218</testSuiteGuid>
   <testCaseLink>
      <guid>0653f6ca-6c90-4708-830f-3896152ca02d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCOUT2-T262-Opponent Scouts Report View</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>83a8ffb4-eb3d-44f7-8d0d-fe27d9e21d4b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCOUT2-T262-TestCase2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>68e9cd96-de10-4991-9f8d-8c26ab6eb313</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCOUT2-T262-TestCase3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bb309a73-90cc-43cd-b440-4dd926621a66</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCOUT2-T288-Plays View</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fbbf60d1-e473-4234-8a42-8d354a489e78</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCOUT2-T289-Playbooks View</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0453563b-08ac-4155-821b-0c9b89df707c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SCOUT2-T287-Videos View</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2734a6fe-bc07-490a-8131-69edd9e786aa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/QA-764-View tiles in Player Scout</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2cabee54-0446-4013-b4c3-7c3be1d4855f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/QA-747-Personnel tile with no text sub-tile</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>27713f3b-28b9-437e-9e6f-88216804ec02</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/QA-765-View tiles in opponent Scout</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b5de1305-0910-4ad9-b884-a7dde0aa0694</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/QA-763-View tiles in Self Scout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
