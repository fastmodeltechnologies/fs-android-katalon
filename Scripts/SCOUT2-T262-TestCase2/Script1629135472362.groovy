import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.util.internal.PathUtil as PathUtil
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory as MobileDriverFactory
import io.appium.java_client.AppiumDriver as AppiumDriver
import io.appium.java_client.MobileElement as MobileElement

MobileBuiltInKeywords.delay(2)

Mobile.tap(findTestObject('Scouts/Air_Force_Falcons', [('scoutName') : findTestData(GlobalVariable.data).getValue('scoutName',1)]), 10)

MobileBuiltInKeywords.delay(2)

'Get Device Height and Store in device_height variable'
device_Height = Mobile.getDeviceHeight()

'Get Width Height and Store in device_Width variable'
device_Width = Mobile.getDeviceWidth()

'Storing the startX value by dividing device width by 2. Because x coordinates are constant for Vertical Swiping'
int startX = device_Width / 2

'Here startX and endX values are equal for vertical Swiping for that assigning startX value to endX'
int endX = startX

'Storing the startY value'
int startY = device_Height * 0.30

'Storing the endY value'
int endY = device_Height * 0.80

'Swipe Vertical from top to bottom'
for (int i = 0; i < 6; i++) {
	Mobile.swipe(startX, endY, endX, startY)

	MobileBuiltInKeywords.delay(2)
}

Mobile.tap(findTestObject('Tiles/Roster_Tile'), 3)

Mobile.waitForElementPresent(findTestObject('Tiles/Roster_Swipe'), 45)

'Get Device Height and Store in device_height variable'
device_Height = Mobile.getDeviceHeight()

'Get Width Height and Store in device_Width variable'
device_Width = Mobile.getDeviceWidth()

'Storing the startY value by dividing device height by 4. Because y coordinates are constant for Horizontal Swiping'
int startY2 = device_Height / 4

'Here startY and endY values are equal for horizontal Swiping for that assigning startY value to endY'
int endY2 = startY2

'Storing the startX value'
int startX2 = device_Width * 0.25

'Storing the endX value'
int endX2 = device_Width * 0.80

'Swipe horizontal from right to left'
for (int i = 0; i < 2; i++) {
	Mobile.swipe(endX2, startY2, startX2, endY2)

	MobileBuiltInKeywords.delay(5)
}

Mobile.pressBack()

Mobile.tap(findTestObject('Tiles/FastScout_Factors_Tile'), 3)

Mobile.waitForElementPresent(findTestObject('Tiles/FastScoutFactors_Swipe'), 45)

Mobile.swipe(endX2, startY2, startX2, endY2)

MobileBuiltInKeywords.delay(5)

Mobile.pressBack()

Mobile.tap(findTestObject('Tiles/Team_Stats_Tile'), 3)

Mobile.waitForElementPresent(findTestObject('Tiles/Team_Stats_Swipe'), 45)

Mobile.swipe(endX2, startY2, startX2, endY2)

MobileBuiltInKeywords.delay(5)

Mobile.pressBack()

'Swipe Vertical from top to bottom'
for (int i = 0; i < 2; i++) {
	Mobile.swipe(startX, endY, endX, startY)

	MobileBuiltInKeywords.delay(2)
}


Mobile.tap(findTestObject('Tiles/Depth_Chart_Tile'), 5)

Mobile.waitForElementPresent(findTestObject('Tiles/DepthChart_Swipe'), 45)

Mobile.swipe(endX2, startY2, startX2, endY2)

MobileBuiltInKeywords.delay(5)

Mobile.pressBack()

Mobile.tap(findTestObject('Tiles/Individual_Boxscore_Tile'), 3)

Mobile.waitForElementPresent(findTestObject('Tiles/IndividualBoxscore_Swipe'), 45)

for (int i = 0; i < 2; i++) {
	Mobile.swipe(endX2, startY2, startX2, endY2)

	MobileBuiltInKeywords.delay(5)
}

Mobile.pressBack()



Mobile.tap(findTestObject('Tiles/Cumulative_Boxscore_Tile'), 3)

Mobile.waitForElementPresent(findTestObject('Tiles/CumulativeBoxscore_Swipe'), 45)

for (int i = 0; i < 2; i++) {
	Mobile.swipe(endX2, startY2, startX2, endY2)

	MobileBuiltInKeywords.delay(5)
}

Mobile.pressBack()

Mobile.tap(findTestObject('Tiles/Clutch_Stats_Tile'), 3)

Mobile.waitForElementPresent(findTestObject('Tiles/ClutchStats_Swipe'), 45)

for (int i = 0; i < 2; i++) {
	Mobile.swipe(endX2, startY2, startX2, endY2)

	MobileBuiltInKeywords.delay(5)
}

Mobile.pressBack()

Mobile.tap(findTestObject('Tiles/Lineup_Stats_Tile'), 3)

Mobile.waitForElementPresent(findTestObject('Tiles/LineupStats_Swipe'), 45)

for (int i = 0; i < 2; i++) {
	Mobile.swipe(endX2, startY2, startX2, endY2)

	MobileBuiltInKeywords.delay(5)
}

Mobile.pressBack()

Mobile.tap(findTestObject('Tiles/AttachmentTab'), 3)

MobileBuiltInKeywords.delay(5)

Mobile.pressBack()

MobileBuiltInKeywords.delay(3)

