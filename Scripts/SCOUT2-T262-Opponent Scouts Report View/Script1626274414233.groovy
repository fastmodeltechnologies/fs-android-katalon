import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.util.internal.PathUtil as PathUtil

MobileBuiltInKeywords.delay(2)

//Mobile.tap(findTestObject('Scouts/Air_Force_Falcons'), 10)

Mobile.tap(findTestObject('Scouts/Air_Force_Falcons', [('scoutName') : findTestData(GlobalVariable.data).getValue("scoutName", 1)]) ,10)

//Mobile.tap(findTestObject('Scouts/Air_Force_Falcons', [('scoutName') : (GlobalVariable.data).getValue("scoutName", 1)]) ,10)

MobileBuiltInKeywords.delay(5)

Mobile.tap(findTestObject('Tiles/Text_Tile'), 5)

MobileBuiltInKeywords.delay(5)

Mobile.pressBack()

Mobile.tap(findTestObject('Tiles/Image_Tile'), 3)

Mobile.pressBack()

Mobile.scrollToText("Team Pace")

Mobile.tap(findTestObject('Tiles/Team_Pace'), 3)

Mobile.pressBack()

Mobile.tap(findTestObject('Tiles/Points_Per_Period'), 3)

Mobile.pressBack()

Mobile.scrollToText("3PT Shooters")

Mobile.tap(findTestObject('Tiles/3PT_Shooters'), 3)

Mobile.pressBack()

MobileBuiltInKeywords.delay(3)

'Get Device Height and Store in device_height variable'
device_Height = Mobile.getDeviceHeight()

'Get Width Height and Store in device_Width variable'
device_Width = Mobile.getDeviceWidth()

'Storing the startX value by dividing device width by 2. Because x coordinates are constant for Vertical Swiping'
int startX3 = device_Width / 2

'Here startX and endX values are equal for vertical Swiping for that assigning startX value to endX'
int endX3 = startX3

'Storing the startY value'
int startY3 = device_Height * 0.30

'Storing the endY value'
int endY3 = device_Height * 0.70

'Swipe Vertical from top to bottom'
Mobile.swipe(startX3, endY3, endX3, startY3)

MobileBuiltInKeywords.delay(2)

Mobile.tap(findTestObject('Tiles/Top_Scorers'), 3)

Mobile.pressBack()

Mobile.tap(findTestObject('Tiles/Turnover_percent'), 3)

Mobile.pressBack()

MobileBuiltInKeywords.delay(3)

Mobile.pressBack()
