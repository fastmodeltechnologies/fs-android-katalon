import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.tap(findTestObject('Scouts/self scout/navigate Tab- SELF'), 0)
Mobile.verifyElementExist(findTestObject('Object Repository/Scouts/self scout/select-self-Scout'), 0)
Mobile.tap(findTestObject('Object Repository/Scouts/self scout/select-self-Scout'), 0)

Mobile.tap(findTestObject('Tiles/Text_Tile'), 0)
Mobile.verifyElementVisible(findTestObject('Object Repository/Scouts/self scout/Text Tile - Details'), 0)
Mobile.pressBack()

Mobile.tap(findTestObject('Tiles/Image_Tile'), 0)
MobileBuiltInKeywords.delay(3)
Mobile.pressBack()

Mobile.tap(findTestObject('Tiles/FastScout_Factors_Tile'), 3)
MobileBuiltInKeywords.delay(5)
Mobile.pressBack() 

'Get Device Height and Store in device_height variable'
device_Height = Mobile.getDeviceHeight()

'Get Width Height and Store in device_Width variable'
device_Width = Mobile.getDeviceWidth()

'Storing the startX value by dividing device width by 2. Because x coordinates are constant for Vertical Swiping'
int startX = device_Width / 2

'Here startX and endX values are equal for vertical Swiping for that assigning startX value to endX'
int endX = startX

'Storing the startY value'
int startY = device_Height * 0.30

'Storing the endY value'
int endY = device_Height * 0.85

Mobile.scrollToText('Team Pace')

Mobile.tap(findTestObject('Object Repository/Scouts/self scout/Fast Scout Chart'), 3)
Mobile.verifyElementExist(findTestObject('Object Repository/Scouts/self scout/FastScout Chart-Details'), 0)
Mobile.pressBack()


Mobile.tap(findTestObject('Tiles/Team_Pace'), 3)
MobileBuiltInKeywords.delay(3)
Mobile.pressBack()

MobileBuiltInKeywords.delay(3)
Mobile.tap(findTestObject('Tiles/Points_Per_Period'), 3)
Mobile.pressBack()

Mobile.scrollToText('Team Stat Comparison')

Mobile.tap(findTestObject('Tiles/Team_Pace_Chart'), 3)
MobileBuiltInKeywords.delay(3)
Mobile.pressBack()

Mobile.tap(findTestObject('Scouts/self scout/Individual Tile'),3)
MobileBuiltInKeywords.delay(3)
Mobile.swipe(startX, endY, endX, startY)
MobileBuiltInKeywords.delay(3)
Mobile.pressBack()

Mobile.tap(findTestObject('Tiles/Cumulative_Boxscore_Tile'), 3)
Mobile.waitForElementPresent(findTestObject('Tiles/CumulativeBoxscore_Swipe'), 45)
MobileBuiltInKeywords.delay(3)
Mobile.pressBack()

Mobile.tap(findTestObject('Tiles/Team_Stat_Comparison'), 3)
Mobile.swipe(startX, endY, endX, startY)
MobileBuiltInKeywords.delay(2)
Mobile.pressBack()

Mobile.scrollToText('Free Throw Shooters')

Mobile.tap(findTestObject('Tiles/Team_Stats_Tile'), 3)
Mobile.waitForElementPresent(findTestObject('Tiles/Team_Stats_Swipe'), 45)
MobileBuiltInKeywords.delay(5)
Mobile.pressBack()

Mobile.tap(findTestObject('Object Repository/Scouts/self scout/Personnel Tile'), 3)
Mobile.swipe(startX, endY, endX, startY)
MobileBuiltInKeywords.delay(2)
Mobile.pressBack()

Mobile.tap(findTestObject('Tiles/3PT_Shooters'), 3)
MobileBuiltInKeywords.delay(2)
Mobile.pressBack()

Mobile.tap(findTestObject('Tiles/Top_Scorers'), 3)
MobileBuiltInKeywords.delay(2)
Mobile.pressBack()

Mobile.tap(findTestObject('Tiles/Top_Rebounders'), 3)
MobileBuiltInKeywords.delay(2)
Mobile.pressBack()

Mobile.tap(findTestObject('Tiles/Free_Throw_Shooters'), 3)
MobileBuiltInKeywords.delay(2)
Mobile.pressBack()

'Swipe Vertical from top to bottom'
for (int i = 0; i < 1; i++) {
	Mobile.swipe(startX, endY, endX, startY)

	MobileBuiltInKeywords.delay(2)
}

Mobile.tap(findTestObject('Tiles/Depth_Chart_Tile'), 5)
Mobile.swipe(startX, endY, endX, startY)
MobileBuiltInKeywords.delay(3)
Mobile.pressBack()

Mobile.tap(findTestObject('Tiles/Recent_Games'), 3)
MobileBuiltInKeywords.delay(3)
Mobile.pressBack()

Mobile.tap(findTestObject('Tiles/Ball_Control'), 3)
MobileBuiltInKeywords.delay(3)
Mobile.pressBack()

'Swipe Vertical from top to bottom'
for (int i = 0; i < 1; i++) {
	Mobile.swipe(startX, endY, endX, startY)

	MobileBuiltInKeywords.delay(2)
}

Mobile.tap(findTestObject('Tiles/Custom_Table'), 3)
MobileBuiltInKeywords.delay(3)
Mobile.pressBack()

Mobile.tap(findTestObject('Tiles/Team_Advanced_Stats'), 3)
MobileBuiltInKeywords.delay(3)
Mobile.pressBack()

Mobile.tap(findTestObject('Tiles/Clutch_Stats_Tile'), 3)
Mobile.waitForElementPresent(findTestObject('Tiles/ClutchStats_Swipe'), 45)
Mobile.pressBack()

'Swipe Vertical from top to bottom'
for (int i = 0; i < 1; i++) {
	Mobile.swipe(startX, endY, endX, startY)

	MobileBuiltInKeywords.delay(5)
}
Mobile.tap(findTestObject('Tiles/Lineup_Stats_Tile'), 3)
Mobile.waitForElementPresent(findTestObject('Tiles/LineupStats_Swipe'), 45)
Mobile.pressBack()

Mobile.tap(findTestObject('Tiles/Defense'), 3)
MobileBuiltInKeywords.delay(2)
Mobile.pressBack()

Mobile.tap(findTestObject('Object Repository/Scouts/self scout/Offensive Shot Chart'), 3)
MobileBuiltInKeywords.delay(3)
Mobile.pressBack()

Mobile.tap(findTestObject('Object Repository/Scouts/self scout/Defensive Shot Chart'), 3)
MobileBuiltInKeywords.delay(3)
Mobile.pressBack()

for (int i = 0; i < 1; i++) {
	Mobile.swipe(startX, endY, endX, startY)

	MobileBuiltInKeywords.delay(5)
}

Mobile.tap(findTestObject('Object Repository/Scouts/self scout/Player Shot Chart'), 3)
MobileBuiltInKeywords.delay(3)
Mobile.pressBack()

MobileBuiltInKeywords.delay(3)
Mobile.pressBack()
