import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration


MobileBuiltInKeywords.delay(5)

Mobile.tap(findTestObject('Videos/videosTab'), 10)

Mobile.tap(findTestObject('Videos/Video_JonathanBarreiro', [('videoName') : findTestData(GlobalVariable.data).getValue("videoName", 8)]) ,5)

//Mobile.tap(findTestObject('Videos/Video_JonathanBarreiro', [('videoName') : (GlobalVariable.data).getValue("videoName", 8)]) ,5)

Mobile.tap(findTestObject('Plays/synergySignInBtn'), 0)

Mobile.setText(findTestObject('Plays/synergyEmail'), GlobalVariable.SynergyEmail, 0)

Mobile.setText(findTestObject('Plays/synergyPassword'), GlobalVariable.SynergyPassword, 0)

Mobile.tap(findTestObject('Plays/synergyLoginBtn'), 10)

MobileBuiltInKeywords.delay(30)

Mobile.pressBack()


MobileBuiltInKeywords.delay(5)

Mobile.scrollToText("SampleVideo__10mb.mp4")

//Mobile.tap(findTestObject('Videos/Video_SampleVideo'), 0)

Mobile.tap(findTestObject('Videos/Video_SampleVideo', [('sampleVideo') : findTestData(GlobalVariable.data).getValue("sampleVideo", 7)]) ,5)
//Mobile.tap(findTestObject('Videos/Video_SampleVideo', [('sampleVideo') : (GlobalVariable.data).getValue("sampleVideo", 7)]) ,5)

MobileBuiltInKeywords.delay(40)

Mobile.pressBack()

Mobile.tap(findTestObject('Plays/FastScout_Logo'), 3)

MobileBuiltInKeywords.delay(2)

Mobile.tap(findTestObject('Plays/Synergy_Signout_Btn'), 0)

MobileBuiltInKeywords.delay(3)



