import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

MobileBuiltInKeywords.delay(2)

Mobile.tap(findTestObject('Scouts/Air_Force_Falcons', [('scoutName') : findTestData(GlobalVariable.data).getValue('scoutName',1)]), 0)

MobileBuiltInKeywords.delay(2)

Mobile.tap(findTestObject('Scouts/self scout/personnel_tile- Joseph Octave'), 0)
MobileBuiltInKeywords.delay(2)

Mobile.verifyElementExist(findTestObject('Scouts/self scout/personnel_tile_details - JOSEPH OCTAVE'), 5)

'Get Device Height and Store in device_height variable'
device_Height = Mobile.getDeviceHeight()

'Get Width Height and Store in device_Width variable'
device_Width = Mobile.getDeviceWidth()

'Storing the startY value by dividing device height by 4. Because y coordinates are constant for Horizontal Swiping'
int startY2 = device_Height / 4

'Here startY and endY values are equal for horizontal Swiping for that assigning startY value to endY'
int endY2 = startY2

'Storing the startX value'
int startX2 = device_Width * 0.25

'Storing the endX value'
int endX2 = device_Width * 0.80


'Swipe horizontal from right to left'
for (int i = 0; i < 2; i++) {
	Mobile.swipe(endX2, startY2, startX2, endY2)

	MobileBuiltInKeywords.delay(5)
}