
Step-1: Download and install Node.js (Link- https://nodejs.org/en/download/)
        Check node.js is installed by running the below commands
            node -v
            npm -v
	If you want to know the location where node.js & npm installed, check with below commands
         where node
         where npm
         (For ex: C:\Users\mgrandhi>where node
		  	      C:\Program Files\nodejs\node.exe(this is the location))

Step-2: Install APPIUM
        Check Java is installed on your system
           java -version
        Then install Appium by running below command
           npm install -g appium
(Note: Appium should be installed without any errors)

        Check if appium is installed through below command
           appium -v
	If you want to know the location where appium is installed, check with below command
          where appium

Step-3: Open Katalon Studio and connect to Appium
        Open Katalon studio and go to Windows menu and go to Katalon Studio Preferences
		Go to Mobile under Katalon section
		Browse the location of appium under Appium Directory and click Apply & OK
		(For ex: C:\Users\mgrandhi\AppData\Roaming\npm\node_modules\appium(Location of appium))
		
Step-4: Setup android mobile device for automation
        Go to settings of mobile device-->System/About-->Developer Options-->Debugging-->Enable USB Debugging
		Make sure to enable USB debugging under Developer Options

Step-5: Connect android device to your system with USB cable or you can use android emulator
		Download Android SDK(Katalon will automatically prompt for this download when try to run the test by selecting android option)
		Check the device is attached properly by running below command under the path '.katalon\tools\android_sdk\platform-tools'
		       adb devices
			It will show the ID of attached devices
		(For Ex: C:\Users\mgrandhi\.katalon\tools\android_sdk\platform-tools>adb devices)
	Or the device will be shown in the Katalon studio when try to run the test by selecting android option

Step-6: Run and validate the mobile test project after recording the test script

		   

